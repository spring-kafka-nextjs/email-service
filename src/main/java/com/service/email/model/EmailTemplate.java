package com.service.email.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "EMAIL_TEMPLATE")
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class EmailTemplate {
    @Id
    private String id;

    private String name;
    private String content;
}
