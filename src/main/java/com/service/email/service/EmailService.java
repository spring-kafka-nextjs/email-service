package com.service.email.service;

import com.service.email.repository.EmailTemplateRepository;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Map;


@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private EmailTemplateRepository emailTemplateRepository;

    public Mono<Void> sendEmail(String username, String to,String subject, String templateName, Map<String, String> placeholders) {
        return emailTemplateRepository.findByName(templateName)
                .flatMap(template -> {
                    String processedContent = replacePlaceholders(username, template.getContent(), placeholders);
                    return sendHtmlEmail(to, subject, processedContent);
                });
    }

    private Mono<Void> sendHtmlEmail(String to, String subject, String htmlContent) {
        return Mono.fromRunnable(() -> {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper;
            try {
                helper = new MimeMessageHelper(mimeMessage, true);
                helper.setTo(to);
                helper.setFrom("mailtrap@stackdevplay.me");
                helper.setSubject(subject);
                helper.setText(htmlContent, true);
                mailSender.send(mimeMessage);
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }).subscribeOn(Schedulers.boundedElastic()).then();
    }

    private String replacePlaceholders(String username, String content, Map<String, String> placeholders) {
        for (Map.Entry<String, String> entry : placeholders.entrySet()) {
            content = content.replace("{{" + entry.getKey() + "}}", "http://localhost:3000/confirmation?username="+username+"&token="+entry.getValue());
        }
        return content;
    }
}
