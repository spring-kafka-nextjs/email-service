package com.service.email.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ConfirmationMessageKafka {
    private String username;
    private String email;
    private String token;
}