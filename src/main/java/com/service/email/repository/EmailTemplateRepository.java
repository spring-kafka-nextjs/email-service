package com.service.email.repository;

import com.service.email.model.EmailTemplate;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface EmailTemplateRepository extends ReactiveMongoRepository<EmailTemplate, String> {

    Mono<EmailTemplate> findByName(String name);
}
