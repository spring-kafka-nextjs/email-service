package com.service.email.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.service.email.dto.ConfirmationMessageKafka;
import com.service.email.service.EmailService;
import com.service.email.utils.DynamicModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class UserListener {

    @Autowired
    private DynamicModelMapper modelMapper;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private EmailService emailService;

    @KafkaListener(topics = "register-confirmation", groupId = "user-group")
    public void registerProduct(String message) {
        modelMapper.mapToModel(message, ConfirmationMessageKafka.class)
                .flatMap(msg -> {
                    System.out.println(message);
                    Map<String, String> placeholders = new HashMap<>();
                    placeholders.put("link", msg.getToken());

                    return emailService.sendEmail(msg.getUsername(), msg.getEmail(),"Subject", "registration", placeholders);
                })
                .doOnError(error -> {
                    try {
                        kafkaProducer.sendMessage("register-user-failed", error.getMessage());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                })
                .subscribe();
    }

}